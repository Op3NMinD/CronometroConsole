﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;



namespace CronometroConsole
{

    public class Cronometro
    {

        Stopwatch cronometro = new Stopwatch();
        
        

        #region Atributos
        private int _tempo;
        private string _mensagem;
        private bool _estado;
        #endregion

    
        #region Propriedades  
        public int Tempo
        {
            get { return _tempo; }
            set { _tempo = value; }
        }
        public string Mensagem
        {
            get { return _mensagem; }
            set { _mensagem = value; }
        }
        public bool Estado
        {
            get { return _estado; }
            set { _estado = value; }
        }

        #endregion


        #region Métodos


        //construtores

        //public Cronometro()
        //{

        //    if (Estado)
        //    {
        //        Mensagem = "Contador iniciado com sucesso!";
        //    }
        //    else
        //    {
        //        Mensagem = "Efetuado reset ao contador!";
        //    }
        //}


        public string mensagem()
        {
            if (Estado)
            {
                Mensagem = "Contador iniciado com sucesso!";
            }
            else
            {
                Mensagem = "Efetuado reset ao contador!";
            }

            return Mensagem;
        }

        public string start()
        {
            
            Estado = true;
            cronometro.Start();
            mensagem();
            return Mensagem;          
        }

        public void stop()
        {
            cronometro.Stop();
        }

        public string reset()
        {
            Estado = false;
            cronometro.Reset();
            mensagem();
            return Mensagem;            
        }

        public int elapsed()
        {
            Tempo = cronometro.Elapsed.Seconds;
            return Tempo;
        }

        #endregion
    }
}
