﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace CronometroConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Cronometro cronometro = new Cronometro();
            char op;

            do
            {
                op = menu();
                switch (op)
                {
                    case '1': cronometro.start(); Console.Clear(); break;
                    case '2':
                        {
                            cronometro.stop();
                            TimeSpan ts = TimeSpan.FromSeconds(cronometro.elapsed());
                            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                               ts.Hours, ts.Minutes, ts.Seconds,
                               ts.Milliseconds / 10);
                            Console.WriteLine("Tempo decorrido:  " + elapsedTime);
                            Console.ReadKey();
                        }
                        break;
                    case '3': cronometro.reset(); break;

                }
            } while (op != '4');       
        }

        static char menu()
        {
            char op;
            Console.WriteLine("****************************");
            Console.WriteLine("                            ");
            Console.WriteLine(" 1 - Iniciar contador       ");
            Console.WriteLine(" 2 - Parar Contador         ");
            Console.WriteLine(" 3 - Reset ao contador      ");
            Console.WriteLine(" 4 - Sair do programa       ");
            Console.WriteLine("                            ");
            Console.WriteLine("****************************");
            op = Convert.ToChar(Console.ReadLine());
            return op;
        }
    }
}
